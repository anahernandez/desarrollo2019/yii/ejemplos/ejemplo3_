﻿CREATE DATABASE 
  ejemplo3yii2;

USE ejemplo3yii2;

CREATE OR REPLACE TABLE noticias(
  id int AUTO_INCREMENT,
  titulo varchar(50),
  texto varchar(50),
  PRIMARY KEY(id)
);

INSERT INTO noticias VALUES
  (1, 'Noticia 1', 'Ejemplo para la noticia 1'),
  (2, 'Noticia 2', 'Ejemplo para la noticia 2'),
  (3, 'Noticia 3', 'Ejemplo para la noticia 3'),
  (4, 'Noticia 4', 'Ejemplo para la noticia 4'),
  (5, 'Noticia 5', 'Ejemplo para la noticia 5');
  